package ar.com.interview.arsjdev.challenge;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import ar.com.interview.service.Album;
import ar.com.interview.service.Comment;
import ar.com.interview.service.Photo;
import ar.com.interview.service.User;


@SpringBootApplication
@RestController
public class ChallengeApplication {

	public static void main(String[] args) {
		SpringApplication.run(ChallengeApplication.class, args);
	}

	// Gets all the users OR an user given an ID
	@GetMapping({"/users","/users/{id}"})
	public String getUsers(@PathVariable(name = "id",required = false) Integer userId) throws JsonProcessingException {
		String uri = "";
		RestTemplate restTemplate = new RestTemplate();
		ObjectMapper mapper = new ObjectMapper();
		
		try {
			//Handling of parameters
			if (userId != null) {
				uri = "https://jsonplaceholder.typicode.com/users/"+userId.toString();
				ResponseEntity<User> responseEntity = restTemplate.getForEntity(uri, User.class);
				User user = responseEntity.getBody();
				return mapper.writeValueAsString(user);
				
		    } else {
		    	uri = "https://jsonplaceholder.typicode.com/users";
		    	ResponseEntity<User[]> responseEntity = restTemplate.getForEntity(uri, User[].class);
		    	List<User> users = Arrays.asList(responseEntity.getBody());
		    	return mapper.writeValueAsString(users);
		    }
		}catch(Exception e) {
			//e.printStackTrace();
			return "{ \"code\": 404, \"message\": \"No registers found for the requested ID\"}";
		}
		
	}

	// Gets all the photos for all the albums of an user ID
	@GetMapping("/users/{id}/photos")
	public String getUserPhotos(@PathVariable(value = "id") long id) throws JsonProcessingException {
		
		List<Photo> userPhotos = new ArrayList<Photo>();
		final String uri = "https://jsonplaceholder.typicode.com/users/"+id+"/albums";
		RestTemplate restTemplate = new RestTemplate();
		
		ResponseEntity<Album[]> responseAlbums = restTemplate.getForEntity(uri,Album[].class);
		Album[] userAlbums = responseAlbums.getBody();
		
		if(userAlbums.length<1) {
			return "{ \"code\": 404, \"message\": \"No registers found for the requested ID\"}";
		}

		for(Album a : userAlbums) {
			String uriForPhotos = "https://jsonplaceholder.typicode.com/albums/"+a.getId()+"/photos";
			ResponseEntity<Photo[]> responsePhotos = restTemplate.getForEntity(uriForPhotos,Photo[].class);
			userPhotos.addAll(Arrays.asList(responsePhotos.getBody()));
		}
	
		ObjectMapper mapper = new ObjectMapper();
		
		return mapper.writeValueAsString(userPhotos);
	}
	
	
	// Gets all the comments. Can filter by email(represents an user) or by name
	@GetMapping("/comments")
	public String getComments(@RequestParam Map<String,String> allParams) throws JsonProcessingException {
		
		final String uri = "https://jsonplaceholder.typicode.com/comments";
		RestTemplate restTemplate = new RestTemplate();
		String email = allParams.get("email");
		String name = allParams.get("name");
		
		ResponseEntity<Comment[]> responseComments = restTemplate.getForEntity(uri,Comment[].class);
		Comment[] commentsArray = responseComments.getBody();
		List<Comment> comments = Arrays.asList(commentsArray);

	    if (email != null) {
	    	comments = comments.stream()
				            .filter(c -> c.getEmail().matches(".*"+email+".*"))
				            .collect(Collectors.toList());
	    	
	    }else if (name != null) {
	    	comments = comments.stream()
		            .filter(c -> c.getName().matches(".*"+name+".*"))
		            .collect(Collectors.toList());
	    }
	    
	    if(comments.size()<1) {
	    	return "{ \"code\": 404, \"message\": \"No registers found for the requested filters\"}";
	    }

	    
	    ObjectMapper mapper = new ObjectMapper();
		return mapper.writeValueAsString(comments);
	}

}
